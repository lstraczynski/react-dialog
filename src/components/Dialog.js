import React, {Component} from 'react';
import './Dialog.css';

class Dialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            opened: props.isOpened
        };
        this.close = this.close.bind(this);
    }

    close(value) {
        if (typeof this.props.onClose === 'function') {
            this.props.onClose(value);
        }
    }

    componentDidMount() {
        this.escHandler = (evt) => {
            if (evt.keyCode === 27 && this.state.opened && this.props.closeOnEsc) {
                this.close(this.props.escPressValue || 'esc');
            }
        };

        window.addEventListener('keyup', this.escHandler);
    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.escHandler);
    }

    componentDidUpdate(prevProps) {
        const newState = {};
        if (this.props.isOpened !== prevProps.isOpened) {
            newState.opened = this.props.isOpened;
        }

        if (Object.keys(newState).length) {
            this.setState(newState);
        }

        if (this.props.isOpened !== prevProps.isOpened && this.props.isOpened && typeof this.props.onOpen === 'function') {
            this.props.onOpen();
        }
    }

    render() {
        return (
            <div onClick={(evt) => this.props.closeOnBackdropClick && evt.target.classList.contains('dialog--opened') && this.close('backdrop')}
            className={`dialog ${this.state.opened ? 'dialog--opened' : 'dialog--closed'}`}>
                <div className="dialog__window">
                    <div className="dialog__header">{this.props.header}</div>
                    <div className="dialog__content">{this.props.children}</div>
                    <div className="dialog__footer">
                        <button className="dialog__btn dialog__btn--ok" onClick={() => this.close(this.props.okClickValue || 'ok')}>{this.props.okButtonLabel || 'OK'}</button>
                        <button className="dialog__btn dialog__btn--cancel" onClick={() => this.close(this.props.cancelClickValue || 'cancel')}>{this.props.cancelButtonLabel || 'Cancel'}</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dialog;