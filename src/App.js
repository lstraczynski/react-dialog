import React from 'react';
import './App.css';
import Dialog from './components/Dialog';

class App extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            dialog: false
        };

        this.openDialog = this.openDialog.bind(this);
        this.dialogCloseHandler = this.dialogCloseHandler.bind(this);
        this.dialogOpenHandler = this.dialogOpenHandler.bind(this);
    }

    openDialog() {
        this.setState({
            dialog: true
        });
    }

    dialogCloseHandler(value) {
        this.setState({dialog: false});
        console.log('Dialog closed with value:', value);
    }

    dialogOpenHandler() {
        console.log('Dialog opened');
    }

    render() {
        return (
            <div className="App">
                <button onClick={this.openDialog}>Open dialog</button>
                <Dialog 
                    header="Question"
                    isOpened={this.state.dialog} 
                    onOpen={this.dialogOpenHandler}
                    onClose={this.dialogCloseHandler}
                    closeOnBackdropClick={true}
                    closeOnEsc={true}>
                    Do you want to remove the customer from database? This operation is irreversible!
                </Dialog>
            </div>
          );
    }
}

export default App;
